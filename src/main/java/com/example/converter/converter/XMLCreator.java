package com.example.converter.converter;

import com.example.converter.model.Sentence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

@Component
public class XMLCreator implements Creator {
    private static final Logger LOGGER = LoggerFactory.getLogger(XMLCreator.class);
    @Value("${output-xml.filepath}")
    private String outputXml;

    @Override
    public void createFile(List<Sentence> sentences) {
        try {
            final Document document = createXmlSchema(sentences);
            createFileFromDocument(document, outputXml);
            LOGGER.info("Xml file created in {}", outputXml);

        } catch (ParserConfigurationException e) {
            LOGGER.error("Exception during creation xml schema. Exception: {}", e.getMessage());
        } catch (TransformerException e) {
            LOGGER.error("Exception during xmk file creation. Exception: {}", e.getMessage());

        }
    }

    private Document createXmlSchema(List<Sentence> sentences) throws ParserConfigurationException {
        final DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document document = documentBuilder.newDocument();
        final Element text = document.createElement("text");
        document.appendChild(text);

        sentences.forEach(sentence -> {
            final Element sentenceTag = document.createElement("sentence");
            text.appendChild(sentenceTag);
            sentence.getWords().forEach(word -> {
                final Element wordTag = document.createElement("word");
                wordTag.appendChild(document.createTextNode(word));
                sentenceTag.appendChild(wordTag);

            });
        });
        return document;
    }

    private void createFileFromDocument(Document document, String outputXml) throws TransformerException {
        final Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(new File(outputXml));
        transformer.transform(new DOMSource(document), result);
    }
}
