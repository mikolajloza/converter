package com.example.converter.converter;

import com.example.converter.model.Sentence;

import java.util.List;

public interface Creator {
    void createFile(List<Sentence> sentences);
}
