package com.example.converter.converter;

import com.example.converter.model.Sentence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class VCSCreator implements Creator {

    private static final Logger LOGGER = LoggerFactory.getLogger(VCSCreator.class);
    @Value("${output-csv.filepath}")
    private String outputVcs;

    @Override
    public void createFile(List<Sentence> sentences) {
        final int maxWordsSize = longestSentenceSize(sentences);
        StringBuilder sb = new StringBuilder();
        try (PrintWriter writer = new PrintWriter(new File(outputVcs))) {
            creteCsvHeader(sb, maxWordsSize);
            writer.write(sb.toString());
            sb.setLength(0);

            createCsvLines(sentences, sb, writer);
            LOGGER.info("Csv file created in {}", outputVcs);

        } catch (FileNotFoundException e) {
            LOGGER.error("Exception during csv file creation: Exception: {}", e.getMessage());
        }
    }

    private void createCsvLines(List<Sentence> sentences, StringBuilder sb, PrintWriter writer) {
        AtomicInteger sentenceNumber = new AtomicInteger(1);
        sentences.forEach(sentence -> {
            if (!sentence.getWords().isEmpty()) {
                sb.append("Sentence").append(sentenceNumber.get())
                        .append(",").append(convertToString(sentence.getWords()))
                        .append(System.lineSeparator());
                writer.write(sb.toString());
                sb.setLength(0);
                sentenceNumber.getAndIncrement();
            }
        });
    }

    public String convertToString(List<String> words) {
        return String.join(",", words);
    }

    private void creteCsvHeader(final StringBuilder sb, final int maxWordsSize) {
        sb.append(",");
        if (maxWordsSize == 0) {
            sb.append("words: ").append(System.lineSeparator());
        } else {
            int i = 1;
            while (i < maxWordsSize) {
                sb.append("word ").append(i).append(", ");
                i++;
            }
            sb.append("word ").append(i).append(System.lineSeparator());
        }

    }

    private int longestSentenceSize(List<Sentence> sentences) {
        return sentences.stream().mapToInt(value -> value.getWords().size()).max().orElse(0);
    }
}
