package com.example.converter;

import com.example.converter.service.ConverterProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConverterApplication implements CommandLineRunner {

    private final ConverterProcessor converterProcessor;

    public ConverterApplication(ConverterProcessor converterProcessor) {
        this.converterProcessor = converterProcessor;
    }

    public static void main(String[] args) {
        SpringApplication.run(ConverterApplication.class, args);
    }

    @Override
    public void run(String... args) {
        converterProcessor.process();
    }
}
