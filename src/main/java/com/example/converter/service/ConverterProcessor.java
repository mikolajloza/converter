package com.example.converter.service;

import com.example.converter.converter.VCSCreator;
import com.example.converter.converter.XMLCreator;
import com.example.converter.model.Sentence;
import com.example.converter.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConverterProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConverterProcessor.class);

    private final XMLCreator xmlCreator;
    private final VCSCreator vcsCreator;
    private final Parser parser;


    public ConverterProcessor(XMLCreator xmlCreator, VCSCreator vcsCreator, Parser parser) {
        this.xmlCreator = xmlCreator;
        this.vcsCreator = vcsCreator;
        this.parser = parser;
    }


    public void process() {
        final List<Sentence> sentences = parser.parseFile();

        if (sentences != null && !sentences.isEmpty()) {
            LOGGER.info("Parsed {} sentences from input file", sentences.size());

            LOGGER.info("Being xml file creation");
            xmlCreator.createFile(sentences);

            LOGGER.info("Being csv file creation");
            vcsCreator.createFile(sentences);
        }
    }
}

