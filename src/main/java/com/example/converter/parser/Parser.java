package com.example.converter.parser;

import com.example.converter.model.Sentence;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class Parser {
    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);
    private static final String FILE_PATH_BIN = "src/main/resources/en-sent.bin";
    @Value("${input.filepath}")
    private String inputFile;

    public List<Sentence> parseFile() {
        final LinkedList<Sentence> sentences = new LinkedList<>();
        final StringBuilder stringBuilder = new StringBuilder();
        LOGGER.info("File: {} being parsed", inputFile);
        try (FileInputStream input = new FileInputStream(inputFile)) {
            final Scanner scanner = new Scanner(input);
            final InputStream is = new FileInputStream(FILE_PATH_BIN);
            final SentenceModel model = new SentenceModel(is);
            final SentenceDetectorME sentenceDetectorME = new SentenceDetectorME(model);
            final Pattern pattern = Pattern.compile("^[^-():;]*$");

            while (scanner.hasNext()) {
                final String line = scanner.nextLine();
                if (line.isBlank())
                    continue;
                stringBuilder.append(line).append(" ");
                final String[] array = sentenceDetectorME.sentDetect(stringBuilder.toString());
                for (String sentence : array) {
                    if (sentence.endsWith(".") || sentence.endsWith("?") || sentence.endsWith("!")) {
                        final List<String> collect = Arrays.stream(sentence.substring(0, sentence.length() - 1).split("[\\s,]+"))
                                .filter(word -> !word.isBlank() && pattern.matcher(word).matches())
                                .sorted(String::compareToIgnoreCase)
                                .collect(Collectors.toList());
                        sentences.add(new Sentence(collect));
                        trimStringBuilder(stringBuilder);
                        stringBuilder.delete(0, sentence.length() + 1);
                    }
                }

            }
        } catch (IOException e) {
            LOGGER.error("Exception appears during parsing file, Exception: {}", e.getMessage());
        }
        return sentences;
    }

    private void trimStringBuilder(StringBuilder stringBuilder) {
        final String trim = stringBuilder.toString().trim();
        stringBuilder.setLength(0);
        stringBuilder.append(trim);
    }
}
